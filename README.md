# Programación Distribuida sobre Grandes Volúmenes de Datos

## Para el uso de este repositorio

Gran parte del material necesario del curso va a estar en este repositorio.

A continuación se detallan las instrucciones para accederlo. Cualquier duda o problema consultar por el Slack de la materia.

### En Windows

Para tener disponible este repositorio en tu maquina de trabajo e instalar el software de la materia hay que instalar el programa [git](https://git-scm.com/download/win).

Despues ejecutar en la terminal:

1. `git config --global core.autocrlf false`
1. `git clone --depth 1 --recursive https://bigdata_famaf@bitbucket.org/bigdata_famaf/diplodatos_bigdata.git`

### En Linux

Seguir las instrucciones en [git](https://git-scm.com/download/linux) según la distribucion Linux que tengas instalada.

Despues ejecutar en la terminal:

```
git clone --depth 1 --recursive https://bitbucket.org/bigdata_famaf/diplodatos_bigdata.git
```

## Software necesario para la materia

Para hacer la materia deberás tener acceso al software que usaremos.

A continuación se explican 4 formas distintas para instalarlo, ordenadas desde la más recomendada:

1. En tu computadora con [Docker](https://www.docker.com/).
1. En tu computadora con [Docker Compose](https://docs.docker.com/compose/)
1. En tu computadora con [Anaconda](https://www.anaconda.com/) (solo Linux y Mac)
1. En nabucodonosor con [Anaconda](https://www.anaconda.com/)

Cualquier duda o problema consultar por el Slack de la materia.

---

### En tu computadora con [Docker](https://www.docker.com/)

Requerimientos: >=8GB ram.

#### Instalación [docker](https://www.docker.com/)

* Instalar docker en tu computadora: https://docs.docker.com/get-docker/

* En una terminal ir al directorio donde se clonó el repo de la materia (con el comando `cd` dentro de la terminal).

* Ir al directorio con la defininición de la imagen en este repo:
```
        cd docker
```

* Crear la imagen ejecutando el comando:
```
        docker build --tag diplodatos/bigdata:2.1 -f ./diplodatos_bigdata/dockerfiles/Dockerfile ./diplodatos_bigdata/context
```
**Nota:** Si tiene Linux puede ejecutar el script `./diplodatos_bigdata/build.sh` en vez del comando anterior.

* Tomarse un café ☕

##### En **Linux**:

* Para arrancar el container con [Zeppelin](https://zeppelin.apache.org/) ejecutar
```
docker run -u $(id -u) -it --rm --hostname localhost -p 8080:8080 -p 4040:4040 \
    -v vols/conf:/opt/zeppelin/conf \
    -v vols/logs:/logs \
    -v vols/notebook:/notebook \
    -v ..:/diplodatos_bigdata \
    -e ZEPPELIN_LOG_DIR='/logs' \
    -e ZEPPELIN_NOTEBOOK_DIR='/notebook' \
    -e ZEPPELIN_INTERPRETER_CONNECT_TIMEOUT=120000\
    --name diplodatos_bigdata diplodatos/bigdata:2.1
```
o puede ejecutar el script `./zeppelin.sh` en vez del comando anterior.

##### En **Windows**:

* Para arrancar el container con [Zeppelin](https://zeppelin.apache.org/) ejecutar
```
docker run -u 1000 -it --rm --hostname localhost -p 8080:8080 -p 4040:4040^
        -v "%CD%\vols\conf:/opt/zeppelin/conf"^
        -v "%CD%\vols\logs:/logs"^
        -v "%CD%\vols\notebook:/notebook"^
        -v "%CD%\..:/diplodatos_bigdata"^
        -e ZEPPELIN_LOG_DIR=/logs^
        -e ZEPPELIN_NOTEBOOK_DIR=/notebook^
        -e ZEPPELIN_INTERPRETER_CONNECT_TIMEOUT=120000^
        --name diplodatos_bigdata diplodatos/bigdata:2.1
```
o puede ejecutar el script `zeppelin.cmd` en vez del comando anterior.

* Para probar [Zeppelin](https://zeppelin.apache.org/) en un navegador ir a [http://localhost:8080]().

  Se tiene que ver la interfaz inicial [Zepelin UI](https://zeppelin.apache.org/docs/0.11.1/quickstart/explore_ui.html).

##### Limitar los recursos en Windows utilizados por Docker Desktop mediante el archivo `.wslconfig`

Docker Desktop en Windows utiliza el Subsistema de Windows para Linux (WSL 2) para ejecutar contenedores de Linux. A veces, es necesario ajustar los recursos asignados a WSL 2, como la memoria RAM, el espacio de intercambio (swap), o los núcleos de CPU, para evitar el uso excesivo de recursos en tu sistema.

Puedes personalizar estos límites creando un archivo de configuración `.wslconfig` en el directorio de tu perfil de usuario en Windows (`C:\Users\TuNombreDeUsuario\.wslconfig`). Al iniciar, WSL 2 leerá este archivo y aplicará las configuraciones especificadas.

###### Pasos para crear el archivo `.wslconfig`:

1. Navega al directorio de tu perfil de usuario en Windows.
2. Crea un archivo de texto llamado `.wslconfig`.
3. Dentro del archivo, puedes definir las configuraciones que prefieras. Un ejemplo de configuración para limitar la memoria RAM y procesadores utilizados por WSL 2 es el siguiente:

```
[wsl2]
memory=5GB       # Limita la memoria RAM asignada a 5 GB
processors=4     # Limita el uso de cores a 4
```

###### Explicación de las opciones de configuración:

- `memory`: Especifica la cantidad máxima de memoria RAM que puede utilizar WSL 2.
- `processors`: Limita el número de núcleos de CPU que WSL 2 puede utilizar.

Estas opciones permiten personalizar el uso de recursos de Docker Desktop, lo que puede ser útil si tu equipo tiene limitaciones de hardware o si quieres evitar que Docker Desktop consuma demasiados recursos.

Para más opciones y detalles adicionales, consulta la [documentación oficial de Microsoft sobre WSL](https://docs.microsoft.com/es-es/windows/wsl/).

---

### En tu computadora con [Docker Compose](https://docs.docker.com/compose/)

El entorno de trabajo tiene 3 módulos principales:

* Apache Zeppelin ([entorno principal de las clase](https://bitbucket.org/bigdata_famaf/diplodatos_bigdata/src/master/))
    * http://localhost:8080/
* Jupyter Notebook + Pyspark (para probar otro entorno)
    * http://localhost:8888/
* Una instancia de Postgresql local (para hacer pruebas de ETL en una base de datos con spark desde Jupyter)

Pasos para levantar todo el entorno con docker compose:

* Instalar docker en tu computadora: https://docs.docker.com/get-docker/

* En una terminal ir al directorio donde se clonó el repo de la materia (con el comando `cd` dentro de la terminal).

* Ir al directorio con la defininición de `docker compose` en el repo de la materia:
```
        cd docker
        cd compose
```

* Ejecutar en ese directorio:

```
$ docker compose up --build
```

* Tomarse un café ☕

* Para probar [Zeppelin](https://zeppelin.apache.org/) en un navegador ir a [http://localhost:8080]().
  Se tiene que ver la interfaz inicial [Zepelin UI](https://zeppelin.apache.org/docs/0.11.1/quickstart/explore_ui.html).

---

### En tu computadora con [Anaconda](https://www.anaconda.com/) (solo Linux y Mac)

* Instalar anaconda si no lo tiene ya instalado (ver https://docs.anaconda.com/miniconda/miniconda-install/).

* Crear un environment anaconda:  
```
        conda create -n python-zeppelin python=3.9
```

* Instalar los paquetes:
```
        conda activate python-zeppelin
        conda install matplotlib pandas plotly
        conda install openjdk=8.0.152
```

* Bajar el archivo zip desde [aquí](http://cs.famaf.unc.edu.ar/~damian/tmp/bigdatasoft.zip) y descomprimir.
Después de descomprimir debe haberse creado un directorio `spark` con el software de la materia.

* Ejecutar desde el directorio `spark` recién creado y en el environment de anaconda:
```sh
        cd zeppelin-0.11.1-bin-all
        bin/zeppelin.sh
```

* En navegador abrir http://localhost:8080.
  Se tiene que ver la interfaz inicial [Zepelin UI](https://zeppelin.apache.org/docs/0.11.1/quickstart/explore_ui.html).

* Hay que instalar también [git](https://git-scm.com).

---

### En nabucodonosor con [Anaconda](https://www.anaconda.com/)

* Hacer ssh a tu cuenta en nabucodonosor

* Instalar [Miniconda](https://www.anaconda.com).
```sh
        wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
        bash Miniconda3-latest-Linux-x86_64.sh
        . .bashrc
```

* Crear un environment anaconda:  
```
        conda create -n python-zeppelin python=3.9
```

* Instalar los paquetes:
```
        conda activate python-zeppelin
        conda install matplotlib pandas plotly
        conda install openjdk=8.0.152
```

* Bajar el archivo zip desde [aquí](http://cs.famaf.unc.edu.ar/~damian/tmp/bigdatasoft.zip) y descomprimir:
```sh
        cd
        wget http://cs.famaf.unc.edu.ar/~damian/tmp/bigdatasoft.zip
        unzip bigdatasoft.zip
``` 
> (debe haberse creado un directorio `spark`)

* Ejecutar los siguientes comandos (esto cambia puerto para que cada usuario use uno distinto):
```sh
        echo $[$UID+8080] # recordar <nuevo puerto> (anotarlo)
        cd spark/zeppelin-0.11.1-bin-all/conf
        sed "s/<value>8080/<value>$[$UID+8080]/" zeppelin-site.xml.template > zeppelin-site.xml
```

* Ejecutar (en el environment de anaconda):
```sh
        cd ..
        bin/zeppelin.sh
```

##### En tu maquina (Linux):

* En terminal hacer port forwarding de ssh:
```sh
        ssh -vCN -L 8080:localhost:<nuevo puerto> -l <login en nabuco> nabucodonosor2.ccad.unc.edu.ar
```

* En navegador abrir http://localhost:8080.
  Se tiene que ver la interfaz inicial a [Zepelin](https://zeppelin.apache.org/docs/0.11.1/quickstart/explore_ui.html).

* Si se quiere ver el SparkUI hay que hacer port forwarding tambien.
    - Despues de ejecutar alguna celda en zeppelin (asi levanta spark) imprimir en otra el puerto que usa SparkUI:
```python
        print(sc.uiWebUrl.split(":")[-1])
```

    - En terminal hacer otro port forwarding de ssh:
```sh
        ssh -vCN -L 4040:localhost:<puerto SparkUI> -l <login en nabuco> nabucodonosor2.ccad.unc.edu.ar
```

##### En tu maquina (Windows):

Hay dos métodos para hacer el port forwarding en Windows:
1. Instalar OpenSSH en windows como se explica en [Instalación de OpenSSH para Windows](https://learn.microsoft.com/es-es/windows-server/administration/openssh/openssh_install_firstuse?tabs=gui#install-openssh-for-windows) y seguir los pasos anteriores para Linux.

2. Usando Putty como se explica en [Create a tunnel using PuTTY](https://learn.microsoft.com/en-us/azure/hdinsight/hdinsight-linux-ambari-ssh-tunnel#useputty).
